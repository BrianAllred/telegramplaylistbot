using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TelegramPlaylistBot.Workers;

namespace TelegramPlaylistBot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CallbackController : ControllerBase
    {
        private readonly ILogger<CallbackController> _logger;
        private readonly ITelegram _telegram;
        private readonly ISpotify _spotify;

        public CallbackController(ITelegram telegram, ISpotify spotify, ILogger<CallbackController> logger)
        {
            _logger = logger;
            _telegram = telegram;
            _spotify = spotify;
        }

        [HttpGet]
        [Route("spotify")]
        public async Task<IActionResult> GetSpotify([FromQuery] string code)
        {
            await _spotify.Authorize(code);
            _telegram.EnableSpotify(_spotify);
            return new OkResult();
        }
    }
}