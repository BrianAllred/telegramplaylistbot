using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SpotifyAPI.Web;
using TelegramPlaylistBot.Utilities;
using TelegramPlaylistBot.Workers;

namespace TelegramPlaylistBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            bool.TryParse(Environment.GetEnvironmentVariable("SPOTIFY_REAUTHORIZE"), out var reauth);

            var config = new EnvironmentConfig
            {
                SpotifyClientId = Environment.GetEnvironmentVariable("SPOTIFY_CLIENT_ID"),
                SpotifySecretId = Environment.GetEnvironmentVariable("SPOTIFY_SECRET_ID"),
                TelegramBotToken = Environment.GetEnvironmentVariable("TG_BOT_TOKEN"),
                SpotifyPlaylistId = Environment.GetEnvironmentVariable("SPOTIFY_PLAYLIST_ID"),
                BaseUri = Environment.GetEnvironmentVariable("BASE_URI"),
                Port = Environment.GetEnvironmentVariable("PORT"),
                AccessToken = Environment.GetEnvironmentVariable("SPOTIFY_ACCESS_TOKEN"),
                RefreshToken = Environment.GetEnvironmentVariable("SPOTIFY_REFRESH_TOKEN"),
                Reauthorize = reauth
            };

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TelegramPlaylistBot", Version = "v1" });
            });

            services.AddSingleton(config);
            var telegram = new Workers.Telegram(config);
            telegram.Start().ConfigureAwait(true);
            services.AddSingleton<ITelegram>(telegram);

            Spotify spotify = null;
            if (config.Reauthorize)
            {
                spotify = Spotify.Reauthorize(config).Result;
                telegram.EnableSpotify(spotify);
            }

            if (spotify != null)
            {
                services.AddSingleton<ISpotify>(spotify);
            }
            else
            {
                services.AddSingleton<ISpotify, Spotify>();

                // Print login request URI to console
                var loginRequest = new LoginRequest(
                    new Uri($"{config.BaseUri}:{config.Port}/callback/spotify"),
                    config.SpotifyClientId,
                    LoginRequest.ResponseType.Code)
                {
                    Scope = new[]
                    {
                        Scopes.PlaylistModifyPrivate, Scopes.PlaylistModifyPublic, Scopes.PlaylistReadCollaborative,
                        Scopes.PlaylistReadPrivate, Scopes.UserReadPrivate
                    }
                };

                Console.WriteLine();
                Console.WriteLine("=> Click the following link to authorize this application:");
                Console.WriteLine(loginRequest.ToUri());
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TelegramPlaylistBot v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
