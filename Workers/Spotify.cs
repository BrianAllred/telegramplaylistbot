﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpotifyAPI.Web;
using TelegramPlaylistBot.Utilities;

namespace TelegramPlaylistBot.Workers
{
    public class Spotify : ISpotify
    {
        private readonly EnvironmentConfig _config;
        private SpotifyClient _client;

        public Spotify(EnvironmentConfig config, SpotifyClient client = null)
        {
            _config = config;
            _client = client;
        }

        public async Task<FullTrack> ParseMessage(string message)
        {
            var startIndex = message.IndexOf("track/", StringComparison.Ordinal) + 6;

            var trackId = message.Substring(startIndex);

            if (trackId.Contains('?'))
            {
                trackId = trackId.Substring(0, trackId.IndexOf('?'));
            }

            return await _client.Tracks.Get(trackId);
        }

        public async Task AddTracksById(IEnumerable<string> trackIds)
        {
            var trackIdList = trackIds.ToList();
            if (trackIdList.Count == 0) return;
            var currentPlaylistTracks = (await GetPlaylistTracks()).ToList();
            var tracksResponse = await _client.Tracks.GetSeveral(new TracksRequest(trackIdList));
            var tracks = tracksResponse.Tracks.Where(x => currentPlaylistTracks.All(y => y.Id != x.Id));

            await _client.Playlists.AddItems(_config.SpotifyPlaylistId,
                new PlaylistAddItemsRequest(tracks.Select(x => x.Uri).ToList()));

        }

        public async Task Authorize(string apiToken)
        {
            var response = await new OAuthClient().RequestToken(
                new AuthorizationCodeTokenRequest(_config.SpotifyClientId, _config.SpotifySecretId, apiToken, new Uri($"{_config.BaseUri}:{_config.Port}/callback/spotify"))
            );

            Console.WriteLine();
            Console.WriteLine(" => Client authorized. Save these tokens and re-run with appropriate variables in order to bypass manual authorization in the future.");
            Console.WriteLine($" => SPOTIFY_ACCESS_TOKEN: {response.AccessToken}");
            Console.WriteLine($" => SPOTIFY_REFRESH_TOKEN: {response.RefreshToken}");
            Console.WriteLine(" => SPOTIFY_REAUTHORIZE: true");

            var clientConfig = SpotifyClientConfig.CreateDefault()
                .WithAuthenticator(new AuthorizationCodeAuthenticator(_config.SpotifyClientId, _config.SpotifySecretId, response));
            _client = new SpotifyClient(clientConfig);
        }

        public static async Task<Spotify> Reauthorize(EnvironmentConfig config)
        {
            var response = new AuthorizationCodeTokenResponse
            {
                AccessToken = config.AccessToken,
                RefreshToken = config.RefreshToken,
                TokenType = "Bearer",
                Scope = $"{Scopes.PlaylistReadPrivate} {Scopes.PlaylistReadCollaborative} {Scopes.PlaylistModifyPrivate} {Scopes.PlaylistModifyPublic} {Scopes.UserReadPrivate}"
            };

            var clientConfig = SpotifyClientConfig.CreateDefault()
                .WithAuthenticator(new AuthorizationCodeAuthenticator(config.SpotifyClientId, config.SpotifySecretId, response));
            var client = new SpotifyClient(clientConfig);

            try
            {
                await client.Playlists.CurrentUsers();

                Console.WriteLine();
                Console.WriteLine(" => Spotify client successfully reauthorized");

                return new Spotify(config, client);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        private async Task<IEnumerable<FullTrack>> GetPlaylistTracks()
        {
            var playlist = await _client.Playlists.Get(_config.SpotifyPlaylistId);
            if (playlist.Tracks != null)
            {
                var currentItems = await _client.PaginateAll(playlist.Tracks);
                return currentItems.Select(x => x.Track as FullTrack);
            }

            return null;
        }
    }
}
