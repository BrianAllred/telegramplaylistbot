﻿using System.Threading.Tasks;

namespace TelegramPlaylistBot.Workers
{
    public interface ITelegram
    {
        public Task Start();
        public void EnableSpotify(ISpotify spotify);
    }
}
