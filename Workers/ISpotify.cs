﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SpotifyAPI.Web;

namespace TelegramPlaylistBot.Workers
{
    public interface ISpotify
    {
        public Task<FullTrack> ParseMessage(string message);
        public Task AddTracksById(IEnumerable<string> trackIds);
        public Task Authorize(string apiToken);
    }
}
