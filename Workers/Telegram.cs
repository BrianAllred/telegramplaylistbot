﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using TelegramPlaylistBot.Utilities;

namespace TelegramPlaylistBot.Workers
{
    public class Telegram : ITelegram
    {
        private readonly TelegramBotClient _client;

        private ISpotify _spotify;
        private bool _started;
        private bool _enableSpotify;

        public Telegram(EnvironmentConfig config)
        {
            _client = new TelegramBotClient(config.TelegramBotToken);
        }

        public async Task Start()
        {
            if (_started) return;

            _started = true;

            var updateReceiver = new QueuedUpdateReceiver(_client);

            updateReceiver.StartReceiving();

            Console.WriteLine();
            Console.WriteLine(" => Telegram bot initialized and listening");

            await foreach (var update in updateReceiver.YieldUpdatesAsync())
            {
                try
                {
                    if (update.Message is { } message)
                    {
                        var lines = message.Text.Split(
                            new[] { "\r\n", "\r", "\n" },
                            StringSplitOptions.RemoveEmptyEntries
                        );

                        if (_enableSpotify)
                        {
                            var trackIds = new List<string>();

                            foreach (var line in lines)
                            {
                                if (line.Contains("spotify") && line.Contains("track"))
                                {
                                    try
                                    {
                                        var track = await _spotify.ParseMessage(line);
                                        trackIds.Add(track.Id);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine($" => Couldn't parse track: {ex}");
                                        Console.WriteLine();
                                        Console.WriteLine($" => Original message: {line}");
                                        Console.WriteLine();
                                    }
                                }
                            }

                            try
                            {
                                await _spotify.AddTracksById(trackIds);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($" => Couldn't add tracks: {ex}");
                                Console.WriteLine();
                                Console.WriteLine($" => Track IDs: {string.Join(',', trackIds)}");
                                Console.WriteLine();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($" => Error reading update: {ex}");
                    Console.WriteLine();
                }
            }
        }

        public void EnableSpotify(ISpotify spotify)
        {
            _spotify = spotify;
            _enableSpotify = true;
        }
    }
}
