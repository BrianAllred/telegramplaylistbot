﻿namespace TelegramPlaylistBot.Utilities
{
    public class EnvironmentConfig
    {
        public string SpotifyClientId { get; set; }
        public string SpotifySecretId { get; set; }
        public string TelegramBotToken { get; set; }
        public string SpotifyPlaylistId { get; set; }
        public string BaseUri { get; set; }
        public string Port { get; set; }
        public bool Reauthorize { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
